﻿using PictureViewerApp.Dto;
using System;
using System.Collections.Generic;
using System.IO;

namespace PictureViewerApp.Services
{

    public class GalleryService : IGalleryService
    {
        private static string ImageFolderPath = AppDomain.CurrentDomain.BaseDirectory + "App_Data/PublicContent/Images";
        public IEnumerable<string> GetImageNames()
        {
            var fileList = new List<string>(); 
            foreach (string fileName in Directory.GetFiles(ImageFolderPath, "*.*"))
            {
                fileList.Add(Path.GetFileName(fileName));                
            }
            return fileList;
        }

        public Stream GetImage(string fileName)
        {
            Stream stream;
            try
            {
                stream = new FileStream(Path.Combine(ImageFolderPath,fileName), FileMode.Open);
            }
            catch (Exception ex)
            {
                throw new Exception("Cannot retrieve the file specified");
            }
            
            return stream;
        }
        public bool UploadImage(ImageUploadDto imageUploadDto)
        {
            try
            {
                string FilePath = Path.Combine(ImageFolderPath, imageUploadDto.name);

                int length = 0;
                using (FileStream writer = new FileStream(FilePath, FileMode.Create))
                {
                    int readCount;
                    var buffer = new byte[8192];
                    while ((readCount = imageUploadDto.image.Read(buffer, 0, buffer.Length)) != 0)
                    {
                        writer.Write(buffer, 0, readCount);
                        length += readCount;
                    }
                }
            }
            catch(Exception e)
            {
                return false;
            }
            return true;
        }
    }
}
