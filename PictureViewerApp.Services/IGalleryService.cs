﻿using PictureViewerApp.Dto;
using System.Collections.Generic;
using System.IO;

namespace PictureViewerApp.Services
{
    public interface IGalleryService
    {
        bool UploadImage(ImageUploadDto imageUploadDto);
        IEnumerable<string> GetImageNames();
        Stream GetImage(string fileName);
    }
}
