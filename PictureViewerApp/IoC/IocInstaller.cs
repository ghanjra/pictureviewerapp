﻿using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using PictureViewerApp.Tasks;
using PictureViewerApp.Services;
using System.Web.Http;

namespace PictureViewerApp.IoC
{
    public class IocInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.Register(
           Classes
               .FromThisAssembly()
               .BasedOn<ApiController>()
               .LifestyleScoped()
           );
            container.Register(Component.For(typeof(IGalleryTask)).ImplementedBy<GalleryTask>().LifeStyle.PerWebRequest);
            container.Register(Component.For(typeof(IGalleryService)).ImplementedBy<GalleryService>().LifeStyle.PerWebRequest);
        }
    }
}