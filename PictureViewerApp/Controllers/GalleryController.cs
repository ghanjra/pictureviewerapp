﻿using PictureViewerApp.Dto;
using PictureViewerApp.Tasks;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Web.Http;

namespace PictureViewerApp.Controllers
{
    public class GalleryController : ApiController
    {
        private IGalleryTask _task;
        public GalleryController(IGalleryTask task)
        {
            _task = task;
        }
        // GET: api/Gallery
        public IEnumerable<string> Get()
        {
            return _task.GetImageNames();
        }


        // GET: api/Gallery
        public HttpResponseMessage Get(string fileName)
        {
            HttpResponseMessage result = new HttpResponseMessage(HttpStatusCode.OK);
            var stream = _task.GetImage(fileName);
            result.Content = new StreamContent(stream);
            result.Content.Headers.ContentType =
                new MediaTypeHeaderValue("application/octet-stream");
            return result;
        }

        // POST: api/Gallery
        public HttpResponseMessage Post()
        {
            var dto = new ImageUploadDto();
            var httpRequest = HttpContext.Current.Request;
            bool valid = true;
            //enforcing one file upload a time
            if (httpRequest.Files.Count == 1)
            {
                var postedFile = httpRequest.Files.Get(0);
                dto.name = postedFile.FileName;
                dto.image = postedFile.InputStream;
                valid = _task.UploadImage(dto);

            }
            else
            {
                valid = false;
            }

            return valid ? Request.CreateResponse(HttpStatusCode.Created, "Upload successful") : Request.CreateResponse(HttpStatusCode.BadRequest, "Please upload valid image file");
        }
    }
}
