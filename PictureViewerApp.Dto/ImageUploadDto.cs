﻿using System.IO;

namespace PictureViewerApp.Dto
{
    public class ImageUploadDto
    {
        public string name;
        public Stream image;
    }
}
