This is a picture viewer application in which you can upload images and then view them. An image can be rotated too within its own canvas.

The web application have very basic User Interface with ASP.Net Navigation Menu at top. The main focus of the web app is to demonstrates the use of SOLID principles and Micro-service architecture.
 
The solution folder contain a Web API(PictureViewerApp\PictureViewerApp) and Website(PictureViewerApp\PictureViewerApp.View).

The WebAPI project is set to run at http://localhost:61773 in IIS Express. If it doesn't run on this port please update WebAPI project's properties to set the port to this one.

To check out sample 
1. Please upload an image using startup menu page(Upload Image menu).
2. After image is successfully uploaded, click on View image menu where Your imgae uploads will be listed.
3. Click on image name to view it in viewer.Also you can rotate an image in image viewer as you like.
4. Close to view the next

Note: You should be connected to internet, as the app uses remote library for jquery and icons.