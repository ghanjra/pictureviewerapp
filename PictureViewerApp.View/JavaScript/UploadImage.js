﻿var uploadSpinner;
$(document).ready(function () {
    uploadSpinner = $('#uploadSpinner').hide();
});
function upload() {
    uploadSpinner.show();
    var data = new FormData(jQuery('form')[0]);
    jQuery.each(jQuery('#file')[0].files, function (i, file) {
        data.append('file[]', file);
    });
    $.ajax({
        url: GallaryApiUrl,
        data: data,
        cache: false,
        contentType: false,
        processData: false,
        type: 'POST'
    }).done(function (data, textStatus, jqXHR) {
        uploadSpinner.hide();
        alert(data);
    })
  .fail(function (jqXHR, textStatus, errorThrown) {
      uploadSpinner.hide();
      alert(jqXHR.responseJSON);
  });
}
