﻿//update the value to change the degree of rotation
var rotateBy = 30;

var rotationDegree = 0;
var image;
$(document).ready(function () {
    var spinner = $('#spinner').show();
    image = $('#image');

    jQuery.support.cors = true;
    $("#dialog").dialog({
        autoOpen: false
    });
    $('#dialog').on('dialogclose', function (event) {
        rotateReset();
    });

    PreviewImage = function (fileName) {
        spinner.show();
        imageDialog = $("#dialog");
        imageTag = $('#image');

        //Set the image src
        imageTag.attr('src', GallaryApiUrl + "?fileName=" + fileName);

        //When the image has loaded, display the dialog
        imageTag.load(function () {
            $('#dialog').dialog({
                modal: true,
                resizable: true,
                draggable: true,
                width: 'auto',
                title: fileName
            });
            spinner.hide();
            $("#dialog").dialog('open');
        });
    }

    $('#fileList').on("click", "tr td a", function (e) {
        e.preventDefault();
        PreviewImage($(this).attr('href'));
    });

    $.ajax(
    {
        type: "GET",
        url: GallaryApiUrl,
        contentType: "application/json;",
        dataType: "json",
        cache: false
    }).done(function (data, textStatus, jqXHR) {
        spinner.hide();
        var html = '';
        $.each(data, function (i, item) {
            html += '<tr><td><a href="' + item + '">' + item + '</a></td>' + '</tr>';
        });
        $('#fileList').append(html);
    }).fail(function (jqXHR, textStatus, errorThrown) {
        spinner.hide();
        alert(jqXHR.responseJSON);
    });
})

function rotateLeft() {
    rotationDegree -= rotateBy;
    image.css("transform", "rotate(" + rotationDegree + "deg)");
    image.css("width", image.height());
}

function rotateRight() {
    rotationDegree += rotateBy;
    image.css("transform", "rotate(" + rotationDegree + "deg)");
    image.css("width", image.height());
}

function rotateReset() {
    rotationDegree = 0;
    $('#image').css({ transform: 'none' });
}