﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="ViewImages.aspx.cs" Inherits="ViewImages" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script src="JavaScript/ViewImages.js"></script>
    
    
    <div id="dialog" title="An Image viewer" class="text-center">
        <div class="dialogChild">
            <img id="image" class="imageControl" />
        </div>
        <div class="dialogChild">
        <a href="#" class="rotateControl" onclick="rotateLeft();"><i class="fa fa-undo"></i>Rotate Left</a>
        <a href="#" class="rotateControl" onclick="rotateReset();"><i class="fa fa-times"></i>Reset rotate</a>
        <a href="#" onclick="rotateRight();"><i class="fa fa-repeat"></i>Rotate Right</a>
        </div>
    </div>
    <div>
    <table id="fileList" border='1'>
        <tr>
            <th>Click to Open Image</th>
        </tr>
    </table>
     </div>
    <div class="margin"><div id="spinner" class="spinner  medium-size"></div></div>
   
</asp:Content>

