﻿using PictureViewerApp.Dto;
using System.Collections.Generic;
using System.IO;

namespace PictureViewerApp.Tasks
{
    public interface IGalleryTask
    {
        IEnumerable<string> GetImageNames();
        Stream GetImage(string fileName);
        bool UploadImage(ImageUploadDto imageUploadDto);
    }
}
