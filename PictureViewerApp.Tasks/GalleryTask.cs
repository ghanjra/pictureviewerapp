﻿using System;
using System.Collections.Generic;
using System.IO;
using PictureViewerApp.Dto;
using PictureViewerApp.Services;
using System.Text.RegularExpressions;

namespace PictureViewerApp.Tasks
{
    public class GalleryTask : IGalleryTask
    {
        private IGalleryService _service;
        private static Regex imageExtensionValidator = new Regex("^.*\\.(?i)(jpg|png|gif|bmp)$");
        public GalleryTask(IGalleryService service)
        {
            _service = service;
        }
       
        public IEnumerable<string> GetImageNames()
        {
            return _service.GetImageNames();
        }

        public Stream GetImage(string fileName)
        {
            return _service.GetImage(fileName);
        }
        public bool UploadImage(ImageUploadDto imageUploadDto)
        {
            if (!imageExtensionValidator.IsMatch(imageUploadDto.name)) return false;
            return _service.UploadImage(imageUploadDto);
        }
    }
}
